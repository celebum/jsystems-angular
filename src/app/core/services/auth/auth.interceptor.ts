import { ErrorHandler, Inject, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, Observable, EMPTY } from 'rxjs';
import { AuthService } from './auth.service';
import { API_URL } from '../../tokens';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private errorHandler: ErrorHandler,
    @Inject(API_URL) private api_url: string,
    private auth: AuthService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const url = (request.url.includes('://')) ? request.url : `${this.api_url}${request.url}`

    const req = request.clone({
      url: url,
      setHeaders: {
        Authorization: `Bearer ${this.auth.getToken()}`
      },
    })


    return next.handle(req).pipe(

      catchError((error: unknown) => {
        // this.errorHandler.handleError(error)
        
        if (!(error instanceof HttpErrorResponse)) {
          throw new Error('Unexpected App Error')
        }
        if (error.status === 401) {
          this.auth.login()
        }

        assertSpotifyServerError(error.error)

        throw new Error(error.error.error.message)
        // return EMPTY
      })
    )
  }
}


// https://github.com/colinhacks/zod#objects

function isSpotifyServerError(res: any): res is SpotifyServerError {
  return 'error' in res && 'message' in res.error && typeof res.error.message === 'string'
}

function assertSpotifyServerError(res: any): asserts res is SpotifyServerError {
  if (!('error' in res && 'message' in res.error && typeof res.error.message === 'string'))
    throw new Error('Unexpected Server Response')
}

interface SpotifyServerError {
  "error": {
    "status": number,
    "message": string
  }
}
