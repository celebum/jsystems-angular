import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumSearchContainer } from './album-search.container';

describe('AlbumSearchContainer', () => {
  let component: AlbumSearchContainer;
  let fixture: ComponentFixture<AlbumSearchContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlbumSearchContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumSearchContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
