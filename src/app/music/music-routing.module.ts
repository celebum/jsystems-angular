import { AlbumSearchContainer } from './containers/album-search/album-search.container';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MusicComponent } from './music.component';
import { AlbumDetailsContainer } from './containers/album-details/album-details.container';

const routes: Routes = [
  {
    path: '',
    component: MusicComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'search' },
      { path: 'search', pathMatch: 'full', component: AlbumSearchContainer },
      { path: 'albums', pathMatch: 'full', component: AlbumDetailsContainer },
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule { }
